"""
DDL - manipulação da tabela
"""

import sqlite3

conn = sqlite3.connect("base.db")
cur = conn.cursor()


sql = """
CREATE TABLE IF NOT EXISTS users (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                  name TEXT NOT NULL,
                                  phone TEXT NOT NULL,
                                  email TEXT UNIQUE NOT NULL)"""

cur.execute(sql)
conn.commit()
conn.close()
