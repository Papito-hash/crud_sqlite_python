import sqlite3


def commit_close(func):
    def decorator(*args):
        conn = sqlite3.connect("base.db")
        cur = conn.cursor()
        sql = func(*args)
        cur.execute(sql)
        conn.commit()
        conn.close()

    return decorator


@commit_close
def db_insert(name, phone, email):
    return """INSERT INTO users(name, phone, email)
        VALUES('{}', '{}', '{}')""".format(
        name, phone, email
    )


@commit_close
def db_update(name, email):
    return """
    UPDATE users SET name = '{}' WHERE email = '{}'
    """.format(
        name, email
    )


@commit_close
def db_delete(email):
    return """
    DELETE FROM users WHERE email = '{}'
    """.format(
        email
    )


def db_select(data, field):
    conn = sqlite3.connect("base.db")
    cur = conn.cursor()
    data = """
    SELECT * FROM users WHERE {} = '{}'""".format(
        field, data
    )
    data = cur.fetchone()
    cur.execute(data)
    conn.close()
    return data
